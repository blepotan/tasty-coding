from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404

from tasty.models import Recipe


def recipe_list(request):
    object_list = Recipe.published.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')

    try:
        recipes = paginator.page(page)
    except PageNotAnInteger:
        recipes = paginator.page(1)
    except EmptyPage:
        recipes = paginator.page(paginator.num_pages)

    return render(request,
                  'tasty/recipe/list.html',
                  {'recipes': recipes,
                   'is_welcome': True})


def recipe_detail(request, year, month, day, category, recipe_slug):
    recipe = get_object_or_404(Recipe,
                               slug=recipe_slug,
                               is_published=True,
                               publish__year=year,
                               publish__month=month,
                               publish__day=day,
                               category=category,)
    page = request.GET.get('page')

    if recipe.category == Recipe.RECIPE_DISH:
        object_list = recipe.parts.all()
        num = 1
    else:
        object_list = recipe.steps.all()
        num = 3

    paginator = Paginator(object_list, num)

    try:
        childs = paginator.page(page)
    except PageNotAnInteger:
        childs = paginator.page(1)
    except EmptyPage:
        childs = paginator.page(paginator.num_pages)

    return render(request,
                  'tasty/recipe/detail.html',
                  {'recipe': recipe,
                   'childs': childs,
                   'page': page})


