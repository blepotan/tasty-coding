from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(is_published=True)


class Recipe(models.Model):
    RECIPE_SNACK = 'code-snack'
    RECIPE_DISH = 'code-dish'
    RECIPE_CATEGORY = (
        (RECIPE_SNACK, 'Code Snack'),
        (RECIPE_DISH, 'Code Dish'),
    )

    title = models.CharField(max_length=200)
    author = models.ForeignKey(User,
                               related_name='recipes',
                               on_delete=models.CASCADE)
    thumbnail = models.URLField(blank=True,
                                null=True,
                                default='https://blackvinka.files.wordpress.com/2021/02/nothing.jpg')
    slug = models.SlugField(max_length=250,
                            unique_for_date='publish')
    description = models.CharField(max_length=200)
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_published = models.BooleanField(default=False)
    category = models.CharField(max_length=50,
                                default=RECIPE_DISH,
                                choices=RECIPE_CATEGORY)

    video_url = models.URLField(blank=True, null=True)
    github_link = models.URLField(blank=True, null=True)

    objects = models.Manager()  # The default manager.
    published = PublishedManager()  # Our custom manager.

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('tasty:recipe_detail',
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day,
                             self.category,
                             self.slug])


class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='ingredients', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    version = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Part(models.Model):
    part_number = models.PositiveIntegerField(default=1)
    name = models.CharField(max_length=200)
    recipe = models.ForeignKey(Recipe,
                               related_name='parts',
                               on_delete=models.CASCADE)

    def __str__(self):
        return self.recipe.title


class Procedure(models.Model):
    procedure_number = models.PositiveIntegerField(default=1)
    part = models.ForeignKey(Part,
                             related_name='procedures',
                             on_delete=models.CASCADE)
    content = models.TextField()
    video_time = models.TimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.procedure_number}'


class Step(models.Model):
    step_number = models.PositiveIntegerField(default=0)
    recipe = models.ForeignKey(Recipe, related_name='steps', on_delete=models.CASCADE)
    content = models.TextField()
    video_time = models.TimeField(blank=True, null=True)

    def __str__(self):
        return self.recipe.title


class Recode(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='recodes', on_delete=models.CASCADE)
    user = models.OneToOneField(User, related_name='projects', on_delete=models.CASCADE)
    github_link = models.URLField()
    description = models.TextField()
    thumbnail = models.ImageField(upload_to='images/recodes/')

    def __str__(self):
        return self.user.username

