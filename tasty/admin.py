from django.contrib import admin
from nested_admin.nested import NestedModelAdmin, NestedStackedInline

from tasty.models import Recipe, Part, Ingredient, Procedure, Step


class IngredientInline(NestedStackedInline):
    model = Ingredient


class ProcedureInline(NestedStackedInline):
    model = Procedure


class PartNestedInline(NestedStackedInline):
    model = Part
    inlines = [ProcedureInline]


class StepInline(NestedStackedInline):
    model = Step


@admin.register(Recipe)
class RecipeAdmin(NestedModelAdmin):
    list_display = ['title',
                    'author',
                    'thumbnail',
                    'publish',
                    'is_published',
                    'category']
    list_filter = ['author',
                   'category',
                   'is_published',]
    search_fields = ['title', 'description']
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ('is_published', 'publish')
    inlines = [IngredientInline, PartNestedInline, StepInline]

    def get_inlines(self, request, obj):
        if obj:
            if obj.category == Recipe.RECIPE_DISH:
                return [IngredientInline, PartNestedInline]
            else:
                return [IngredientInline, StepInline]
        return self.inlines




