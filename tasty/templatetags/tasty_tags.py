from django import template
from django.utils.safestring import mark_safe
import markdown

register = template.Library()


@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text, extensions=['fenced_code',
                                                         'codehilite',
                                                         'attr_list',
                                                         'legacy_attrs']))


@register.inclusion_tag('tasty/recipe/card.html')
def recipe_card(recipe, page):
    parts = recipe.parts.all()
    try:
        page = int(page)
    except:
        page = 1

    return {'parts': parts, 'recipe': recipe, 'page': page}