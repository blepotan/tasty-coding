from django.urls import path

from tasty import views

app_name = 'tasty'

urlpatterns = [
    path('', views.recipe_list, name='recipe_list'),
    path('<int:year>/<int:month>/<int:day>/<str:category>/<slug:recipe_slug>/',
         views.recipe_detail,
         name='recipe_detail'),
]

